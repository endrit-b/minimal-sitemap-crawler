
from crawler import Crawler
import argparse

parser = argparse.ArgumentParser(description='This is a crawler destined to create a site map')

parser.add_argument('--domain', action="store", default="", required=True, help="Targeted website (ie: http://example.com)")
parser.add_argument('--output', action="store", default="", help="Site map output file name (default is: sitemap.xml)")

args = parser.parse_args()

print()

if __name__ == '__main__':
    args = args.__dict__

    Crawler(**args).start()
