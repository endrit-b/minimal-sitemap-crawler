# encoding=utf-8

from .utils import XML_HEADER, XML_FOOTER
from urllib.parse import urljoin, urlparse
from urllib.request import urlopen, Request
from urllib.robotparser import RobotFileParser
import re
from datetime import datetime


class Crawler:
    def __init__(self, domain, output, robot_parser=True):
        self.domain = domain
        self.output_fn = output if (output and output.endswith('.xml')) else 'sitemap.xml'  # given file name
        # TODO: make this list extensible with extensions passed from a terminal argument (csv), ie: --exclude -> []
        self.excluded_resources = (".mp3", ".mp4", ".jpg", ".jpeg", ".png", ".gif", ".pdf", ".tar", ".zip")

        # crawling status variables
        self.to_crawl = set([self.domain])
        self.crawled = set([])
        self.exclude = set([])
        self.excluded = set([])

        # Let's define the patterns we'll be looking to crawl, <a href='*'>, <img src='*'>, windows.location='*'
        self.link_pattern = re.compile(b'<a [^>]*href=[\'|"](.*?)[\'"][^>]*?>')
        self.wl_pattern = re.compile(b'.*?window\.location\s*=\s*\"([^"]+)\"')

        # let's declare this reporting container
        self.response_code = {}

        try:
            self.targeted_domain = urlparse(domain).netloc
        except:
            raise ("Invalid domain")

    def start(self):
        self._output_file = open(self.output_fn, 'w')

        # write sitemap xml header
        print(XML_HEADER, file=self._output_file)

        # init Robot instance
        self.init_robot_parser()

        while len(self.to_crawl) != 0:
            self.__crawl_for_links()

        print(XML_FOOTER, file=self._output_file)

        # close the file stream
        self._output_file.close()

    def __crawl_for_links(self):
        crawling_url = self.to_crawl.pop()

        url = urlparse(crawling_url)
        self.crawled.add(crawling_url)

        request = Request(crawling_url, headers={"User-Agent": "Sitemap Crawler"})

        # Ignore resources listed in the excluded_resources, to avoid downloading files like pdf… etc
        if not url.path.endswith(self.excluded_resources):
            try:
                response = urlopen(request)
            except Exception as e:
                print("{1} ===> {0}".format(e, crawling_url))
                return self.__continue_crawling()
        else:
            print("{0} it is excluded, as not wan".format(crawling_url))
            response = None

        # Read the response
        if response is not None:
            try:
                resp_content = response.read()
                response.close()
            except Exception as e:
                print("{1} ===> {0}".format(e, crawling_url))
                return None
        else:
            # Response is None, content not downloaded, just continue and add
            # the link to the sitemap
            resp_content = "".encode()

        lastmod = "<lastmod>" + str(datetime.now()) + "</lastmod>"

        print ("<url><loc>" + self.ascii_special_chars_parser(url.geturl()) + "</loc>" + lastmod + "</url>",
               file=self._output_file)

        if self._output_file:
            self._output_file.flush()

        # Found links
        wl_links = self.wl_pattern.findall(resp_content)
        links = self.link_pattern.findall(resp_content)
        if wl_links:
            links.extend(wl_links)
        for link in links:

            link = link.decode("utf-8")
            print("Found : {0}".format(link))

            # let's validate links, and sanitize if needed
            if link.startswith('/'):
                link = url.scheme + '://' + url.netloc + link
            elif link.startswith('#'):
                link = url.scheme + '://' + url.netloc + url.path + link
            elif not link.startswith(('http', "https")):
                link = url.scheme + '://' + url.netloc + '/' + link

            # Remove the anchor part if needed
            if "#" in link:
                link = link[:link.index('#')]

            # Parse the url to get domain and file extension
            parsed_link = urlparse(link)
            domain_link = parsed_link.netloc

            # if discovered link obeys any of the following rules, skip iteration
            if link in self.crawled:
                continue
            if link in self.to_crawl:
                continue
            if link in self.excluded:
                continue
            if domain_link != self.targeted_domain:
                continue
            if "javascript" in link:
                continue
            if parsed_link.path.startswith(("data:", "mailto:")):
                continue

            # Check if the navigation is allowed by the robots.txt
            if not self.robot_can_fetch(link):
                self.exclude_link(link)
                continue

            self.to_crawl.add(link)

        return None

    def __continue_crawling(self):
        if self.to_crawl:
            self.__crawl_for_links()

    def exclude_link(self, link):
        if link not in self.excluded:
            self.excluded.add(link)

    def init_robot_parser(self):
        try:
            robots_url = urljoin(self.domain, "robots.txt")
            # we use this to module to make sure resources are reachable
            # for more check out this link: http://docs.w3cub.com/python~3.5/library/urllib.robotparser/
            self.rp = RobotFileParser()

            self.rp.set_url(robots_url)
            self.rp.read()
        except Exception as e:
            raise "Make sure to put a valid url. Check if URL scheme is missing (http://)"

    def robot_can_fetch(self, link):
        if self.rp.can_fetch("*", link):
            return True
        else:
            print("{0} was disabled by robots.txt".format(link))
            return False

    def exclude_url(self, link):
        for ex in self.exclude:
            if ex in link:
                return False
        return True

    def ascii_special_chars_parser(self, text):
        return text.replace("&", "&amp;").replace('"', "&quot;").replace("<", "&lt;").replace(">", "&gt;")
