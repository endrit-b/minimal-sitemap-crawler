### What is this?
This is a minimal crawler, destined to crawl and generate a sitemap for a given website.

### Prerequisites

* Debian-based OS (I used Ubuntu)
* Python3
* The rest will be setup by setup.sh file (see Installation section)

### Installation ###
To create a python3 environment, execute the following command:
```bash
$ bash setup.sh
```

### Run script ###
To run the crawler you can simple write this command in your terminal:

```bash
$ bash run_crawler.sh http://example.com sitemap.xml
```

The output of this command will be, all the crawled links for the given website, besides the ones that are excluded based on the rules I defined (robot.txt). In addition, it will generate a sitemap XML file consisting of the URL location and last modified date.